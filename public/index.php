<?php

use App\Kernel;

require __DIR__.'/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/../');
$dotenv->load();

$kernel = new Kernel();

$response = $kernel->start();
$response->send();
