<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController
{
    private UserService $service;

    public function __construct()
    {
        $this->service = new UserService();
    }

    public function list(): JsonResponse
    {
        $users = $this->service->getAll();

        return new JsonResponse([$users], 200);
    }
}
