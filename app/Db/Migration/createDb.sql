create table public.users
(
    id   integer default nextval('"Users_id_seq"'::regclass) not null,
    name varchar(255)
);

alter table public.users
    owner to admin;
