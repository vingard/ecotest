<?php

declare(strict_types=1);

namespace App;

use App\Exceptions\HttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class Kernel
{
    private Request $request;

    /**
     * @var array<string, mixed>
     */
    private array $routes;

    public function __construct()
    {
        $this->request = Request::createFromGlobals();
        $this->routes = require_once(__DIR__.'/Routes/routes.php');
    }

    /**
     * @throws HttpException
     */
    public function start(): JsonResponse
    {
        $routeParams = $this->processRoute();
        $classMethod = $routeParams['classMethod'];

        return (new $routeParams['class'])->$classMethod();
    }

    /**
     * @return array<string, mixed>
     * @throws HttpException
     */
    private function processRoute(): array
    {
        $urlParsed  = parse_url($this->request->getRequestUri());

        $uriParts = explode('/', $urlParsed['path']);

        $baseUri = $uriParts[1];

        if (!array_key_exists($baseUri, $this->routes)) {
            throw new HttpException('Route not found');
        }

        array_shift($uriParts);
        array_shift($uriParts);

        $uriRoute = implode('/', $uriParts);

        if (!array_key_exists($uriRoute, $this->routes[$baseUri])) {
            throw new HttpException('Route not found');
        }

        $routeParams = $this->routes[$baseUri][$uriRoute];

        if ($routeParams['method'] !== $this->request->getMethod()) {
            throw new HttpException('Wrong request method');
        }

        return $this->routes[$baseUri][$uriRoute];
    }
}
