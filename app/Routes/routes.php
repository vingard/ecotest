<?php

declare(strict_types=1);

use App\Controller\UserController;

return [
    'v1' => [
        'user/list' => [
            'class' => UserController::class,
            'classMethod' => 'list',
            'method' => 'GET',
        ],
    ],
];
