<?php

declare(strict_types=1);

namespace App\Repository;
use PDO;

class UserRepository extends BaseRepository
{
    public function findAllUsers(): array
    {
     $query = $this->connection->query('Select * from users');
     $query->execute();

     return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
