<?php

declare(strict_types=1);

namespace App\Repository;

use PDO;
use PDOException;

class BaseRepository
{
    protected PDO $connection;

    public function __construct()
    {
        // TODO check if exists
        $host = $_ENV['DB_HOST'];
        $db = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $pass = $_ENV['DB_PASSWORD'];

        $dsn = sprintf('pgsql:host=%s;port=5432;dbname=%s;', $host, $db);

        try {
            $this->connection = new PDO($dsn, $user, $pass, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
