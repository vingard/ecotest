SHELL=/bin/bash

UID := $(shell id -u)
PWD := $(shell pwd)

build: net start-backend
start: start-backend
down: down-backend
restart: down-backend start-backend

down-backend:
	env UID=${UID} docker-compose down
net:
	docker network create eco-net

start-backend:
	env UID=${UID} docker-compose -f ./docker-compose.yml up -d --build --remove-orphans --force-recreate

composer:
	env UID=${UID} docker-compose run eco-composer

composer-install:
	env UID=${UID} docker-compose run eco-composer install --no-interaction --optimize-autoloader --ignore-platform-reqs
	$(MAKE) restart

composer-update:
	env UID=${UID} docker-compose run eco-composer update --no-interaction --optimize-autoloader --ignore-platform-reqs
	$(MAKE) restart

composer-require:
	env UID=${UID} docker-compose run eco-composer require $(package) --no-interaction --optimize-autoloader --ignore-platform-reqs
	$(MAKE) restart

composer-require-dev:
	env UID=${UID} docker-compose run eco-composer require --dev $(package) --no-interaction --optimize-autoloader --ignore-platform-reqs
	$(MAKE) restart


