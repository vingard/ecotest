# ecotest
Тестовый проект


## Установка

1-Клонируем проект:
```
git clone git@gitlab.com:vingard/ecotest.git
cd ecotest
```

2-Сборка проекта:

Для пользователей Linux:
```
make build
```
для других ОС:
```
docker-compose -f ./docker-compose.yml up -d --build --remove-orphans --force-recreate
```

3-Установка зависимостей
Для пользователей Linux:
```
make composer-install
```
для других ОС:
```
docker-compose run eco-composer
composer install
```

4-Создание БД и примерные данные в  папке Db

5-Результат:
примитивный запрос можно увидеть по адресу
http://localhost:8181/v1/user/list

5-Что осталось за кадром:
1-Использование ОРМ
2-Валидация входных данных
3-Тесты
4-DI
